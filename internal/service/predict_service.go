package service

import "errors"

const (
	AggregateCountryKey  = "country"
	AggregateCampaignKey = "campaign"

	csvLTVSourceFileKey  = "csv"
	jsonLTVSourceFileKey = "json"

	predictLinearExtrapolationModelKey = 0
	predictAbsoluteGrowthModelKey      = 1
	predictHoltModelKey                = 2
)

var ErrAggregateKeyNotSupported = errors.New("this aggregate key not supported")
var errSourceFileKeyNotSupported = errors.New("this source file key not supported")
var errPredictModelKeyNotSupported = errors.New("this predict model key not supported")
var errLowerLimitPredictedDay = errors.New("predicted day cannot be less than 8")

type Predictor interface {
	PredictByAbsoluteGrowthModel(revenueList RevenueList, predictedDay int) PredictList
	PredictByHoltModel(revenueList RevenueList, predictedDay int) PredictList
	PredictByLinearExtrapolationModel(revenueList RevenueList, predictedDay int) PredictList
}

type DataParser interface {
	ParseRevenueFromSourceFile(aggregateKey string) (RevenueList, error)
}

type Service struct {
	predictor     Predictor
	jsonLTVParser DataParser
	csvLTVParser  DataParser
}

type RevenueList map[string][7]float64
type PredictList map[string]float64

func NewLTVPredictService(predictor Predictor, jsonLTVParser DataParser, csvLTVParser DataParser) *Service {
	return &Service{
		predictor:     predictor,
		jsonLTVParser: jsonLTVParser,
		csvLTVParser:  csvLTVParser,
	}
}

func (s *Service) Predict(aggregateKey string, modelPredictKey int, sourceFileKey string, predictedDay int) (PredictList, error) {
	if predictedDay < 8 {
		return nil, errLowerLimitPredictedDay
	}

	var revenueList RevenueList

	switch sourceFileKey {
	case jsonLTVSourceFileKey:
		{
			var err error
			revenueList, err = s.jsonLTVParser.ParseRevenueFromSourceFile(aggregateKey)
			if err != nil {
				return nil, err
			}
		}
	case csvLTVSourceFileKey:
		{
			var err error
			revenueList, err = s.csvLTVParser.ParseRevenueFromSourceFile(aggregateKey)
			if err != nil {
				return nil, err
			}
		}
	default:
		return nil, errSourceFileKeyNotSupported
	}

	switch modelPredictKey {
	case predictLinearExtrapolationModelKey:
		return s.predictor.PredictByLinearExtrapolationModel(revenueList, predictedDay), nil
	case predictAbsoluteGrowthModelKey:
		return s.predictor.PredictByAbsoluteGrowthModel(revenueList, predictedDay), nil
	case predictHoltModelKey:
		return s.predictor.PredictByHoltModel(revenueList, predictedDay), nil
	}
	return nil, errPredictModelKeyNotSupported
}
