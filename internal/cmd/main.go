package main

import (
	"flag"
	"fmt"
	"github.com/jinzhu/configor"
	"github.com/sirupsen/logrus"
	"gitlab.com/Yud_Jin/ltv-predictor/internal/config"
	"gitlab.com/Yud_Jin/ltv-predictor/internal/csvparser"
	"gitlab.com/Yud_Jin/ltv-predictor/internal/jsonparser"
	"gitlab.com/Yud_Jin/ltv-predictor/internal/predictor"
	"gitlab.com/Yud_Jin/ltv-predictor/internal/service"
)

var modelFlag = flag.Int("model", 0, "")
var sourceFileFlag = flag.String("source", "json", "")
var aggregateFlag = flag.String("aggregate", "campaign", "")

var cfg config.Config

func main() {
	flag.Parse()

	if err := configor.Load(&cfg, "etc/config.yml"); err != nil {
		logrus.Fatalf("load config: %s", err)
	}

	jsonLTVParser := jsonparser.NewLTVParser(cfg.SourceJSONFilePath)
	csvLTVParser := csvparser.NewLTVParser(cfg.SourceCSVFilePath)
	revenuePredictor := predictor.NewPredictor(cfg.SmoothingCofA, cfg.SmoothingCofB)
	predictService := service.NewLTVPredictService(revenuePredictor, jsonLTVParser, csvLTVParser)

	predict, err := predictService.Predict(*aggregateFlag, *modelFlag, *sourceFileFlag, cfg.TargetPredictDay)
	if err != nil {
		logrus.Error(err)
	}

	for k, v := range predict {
		fmt.Printf("%s: %v\n", k, v)
	}
}
