package predictor

import (
	"gitlab.com/Yud_Jin/ltv-predictor/internal/service"
	"math"
	"sync"
)

type predictor struct {
	exponentialHoltSmoothingCoefficientA float64
	exponentialHoltSmoothingCoefficientB float64

	wg sync.WaitGroup
}

type PredictListWrapper struct {
	predictList service.PredictList
	m           *sync.Mutex
}

func NewPredictor(smoothingCofA float64, smoothingCofB float64) *predictor {
	return &predictor{exponentialHoltSmoothingCoefficientA: smoothingCofA, exponentialHoltSmoothingCoefficientB: smoothingCofB,
		wg: sync.WaitGroup{}}
}

func (p *predictor) PredictByAbsoluteGrowthModel(revenueList service.RevenueList, predictedDay int) service.PredictList {
	out := PredictListWrapper{
		predictList: make(service.PredictList, len(revenueList)),
		m:           &sync.Mutex{},
	}

	for key, record := range revenueList {
		p.wg.Add(1)
		go p.absoluteGrowth(record, predictedDay, key, out)
	}

	p.wg.Wait()
	return out.predictList
}

func (p *predictor) PredictByLinearExtrapolationModel(revenueList service.RevenueList, predictedDay int) service.PredictList {
	out := PredictListWrapper{
		predictList: make(service.PredictList, len(revenueList)),
		m:           &sync.Mutex{},
	}

	for key, record := range revenueList {
		p.wg.Add(1)
		go p.linearExtrapolation(record, predictedDay, key, out)
	}

	p.wg.Wait()
	return out.predictList
}

func (p *predictor) PredictByHoltModel(revenueList service.RevenueList, predictedDay int) service.PredictList {
	out := PredictListWrapper{
		predictList: make(service.PredictList, len(revenueList)),
		m:           &sync.Mutex{},
	}

	for key, record := range revenueList {
		p.wg.Add(1)
		go p.exponentialHoltSmoothing(record, predictedDay, key, out)
	}

	p.wg.Wait()
	return out.predictList
}

func (p *predictor) linearExtrapolation(arrY [7]float64, predictedDay int, key string, out PredictListWrapper) {
	defer p.wg.Done()

	arrX := [7]float64{1, 2, 3, 4, 5, 6, 7}

	n := len(arrX)

	arrXY := [7]float64{}
	arrX2 := [7]float64{}

	for i := 0; i < n; i++ {
		arrXY[i] = arrX[i] * arrY[i]
		arrX2[i] = math.Pow(arrX[i], 2)
	}

	a := (float64(n)*sumElemArr(arrXY) - sumElemArr(arrX)*sumElemArr(arrY)) / (float64(n)*sumElemArr(arrX2) - math.Pow(sumElemArr(arrX), 2))

	b := (sumElemArr(arrY) - a*sumElemArr(arrX)) / float64(n)

	result := a*float64(predictedDay) + b

	out.m.Lock()
	out.predictList[key] = result
	out.m.Unlock()
}

func (p *predictor) absoluteGrowth(ltv7 [7]float64, predictedDay int, key string, out PredictListWrapper) {
	defer p.wg.Done()

	ltv := make([]float64, predictedDay, predictedDay)
	for i, v := range ltv7 {
		ltv[i] = v
	}

	for i := 6; i < predictedDay-1; i++ {
		ltv[i+1] = ltv[i] + (ltv[i]-ltv[0])/float64(i+1)
	}

	result := ltv[predictedDay-1]

	out.m.Lock()
	out.predictList[key] = result
	out.m.Unlock()
}

func (p *predictor) exponentialHoltSmoothing(ltv7 [7]float64, predictedDay int, key string, out PredictListWrapper) {
	defer p.wg.Done()

	var exp [7]float64
	var trend [7]float64

	exp[0] = ltv7[0]
	trend[0] = 0

	for i := 1; i < 7; i++ {
		exp[i] = p.exponentialHoltSmoothingCoefficientA*ltv7[i] + (1-p.exponentialHoltSmoothingCoefficientA)*(exp[i-1]-trend[i-1])
		trend[i] = p.exponentialHoltSmoothingCoefficientB*(exp[i]-exp[i-1]) + (1-p.exponentialHoltSmoothingCoefficientB)*trend[i-1]
	}

	result := exp[6] + (float64(predictedDay)-7)*trend[6]

	out.m.Lock()
	out.predictList[key] = result
	out.m.Unlock()
}

func sumElemArr(arr [7]float64) float64 {
	var out float64
	for _, v := range arr {
		out += v
	}
	return out
}
