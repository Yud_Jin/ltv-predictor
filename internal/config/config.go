package config

type Config struct {
	SourceJSONFilePath string  `yaml:"source_json_file_path"`
	SourceCSVFilePath  string  `yaml:"source_csv_file_path"`
	SmoothingCofA      float64 `yaml:"exponential_holt_smoothing_coefficient_a"`
	SmoothingCofB      float64 `yaml:"exponential_holt_smoothing_coefficient_b"`
	TargetPredictDay   int     `yaml:"target_predict_day"`
}
