package jsonparser

import (
	"encoding/json"
	"fmt"
	"gitlab.com/Yud_Jin/ltv-predictor/internal/service"
	"io/ioutil"
)

type ltvJson struct {
	CampaignID string
	Country    string
	Users      int
	Ltv1       float64
	Ltv2       float64
	Ltv3       float64
	Ltv4       float64
	Ltv5       float64
	Ltv6       float64
	Ltv7       float64
}

type LTV struct {
	CampaignID string
	Country    string
	Users      int
	LTV        [7]float64
}

type ltvParser struct {
	sourceFilePath string
}

func NewLTVParser(sourceFilePath string) *ltvParser {
	return &ltvParser{sourceFilePath: sourceFilePath}
}

func (p *ltvParser) ParseRevenueFromSourceFile(aggregateKey string) (service.RevenueList, error) {
	data, err := p.parseSourceFile()
	if err != nil {
		return nil, err
	}
	switch aggregateKey {
	case service.AggregateCountryKey:
		return toCountryRevenueList(data), nil

	case service.AggregateCampaignKey:
		return toCampaignRevenueList(data), nil

	}
	return nil, service.ErrAggregateKeyNotSupported
}

func (p *ltvParser) parseSourceFile() ([]LTV, error) {
	var ltvs []ltvJson
	data, err := ioutil.ReadFile(p.sourceFilePath)
	if err != nil {
		return nil, fmt.Errorf("read json file %s", err)
	}
	if err := json.Unmarshal(data, &ltvs); err != nil {
		return nil, err
	}

	out := make([]LTV, 0, len(ltvs))
	for _, ltv := range ltvs {
		out = append(out, LTV{
			CampaignID: ltv.CampaignID,
			Country:    ltv.Country,
			Users:      ltv.Users,
			LTV:        [7]float64{ltv.Ltv1, ltv.Ltv2, ltv.Ltv3, ltv.Ltv4, ltv.Ltv5, ltv.Ltv6, ltv.Ltv7},
		})
	}
	return out, nil
}

func toCampaignRevenueList(ltvs []LTV) service.RevenueList {
	revenueList := make(service.RevenueList)

	for _, ltv := range ltvs {
		if revenue, ok := revenueList[ltv.CampaignID]; !ok {
			var revenue [7]float64
			for i := 0; i < 7; i++ {
				revenue[i] = ltv.LTV[i] * float64(ltv.Users)
			}
			revenueList[ltv.CampaignID] = revenue
		} else {
			for i := 0; i < 7; i++ {
				revenue[i] += ltv.LTV[i] * float64(ltv.Users)
			}
			revenueList[ltv.CampaignID] = revenue
		}
	}
	return revenueList
}

func toCountryRevenueList(ltvs []LTV) service.RevenueList {
	revenueList := make(service.RevenueList)

	for _, ltv := range ltvs {
		if revenue, ok := revenueList[ltv.Country]; !ok {
			var revenue [7]float64
			for i := 0; i < 7; i++ {
				revenue[i] = ltv.LTV[i] * float64(ltv.Users)
			}
			revenueList[ltv.Country] = revenue
		} else {
			for i := 0; i < 7; i++ {
				revenue[i] = ltv.LTV[i] * float64(ltv.Users)
			}
			revenueList[ltv.Country] = revenue
		}
	}
	return revenueList
}
