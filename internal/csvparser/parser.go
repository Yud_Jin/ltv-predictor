package csvparser

import (
	"encoding/csv"
	"fmt"
	"gitlab.com/Yud_Jin/ltv-predictor/internal/service"
	"io"
	"os"
	"strconv"
)

type LTV struct {
	UserId     string
	CampaignID string
	Country    string
	LTV        [7]float64
}

type ltvParser struct {
	sourceFilePath string
}

func NewLTVParser(sourceFilePath string) *ltvParser {
	return &ltvParser{sourceFilePath: sourceFilePath}
}

func (p *ltvParser) ParseRevenueFromSourceFile(aggregateKey string) (service.RevenueList, error) {
	data, err := p.parseSourceFile()
	if err != nil {
		return nil, err
	}
	switch aggregateKey {
	case service.AggregateCountryKey:
		return toCountryRevenueList(data), nil
	case service.AggregateCampaignKey:
		return toCampaignRevenueList(data), nil

	}
	return nil, service.ErrAggregateKeyNotSupported
}

func (p *ltvParser) parseSourceFile() ([]LTV, error) {
	var out []LTV

	file, err := os.Open(p.sourceFilePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	reader := csv.NewReader(file)
	reader.FieldsPerRecord = 10
	for {
		record, err := reader.Read()
		if err != nil {
			if err != io.EOF {
				return nil, fmt.Errorf("read csv file: %s", err)
			} else {
				break
			}
		}

		var ltv [7]float64
		ltv[0], _ = strconv.ParseFloat(record[3], 64)
		ltv[1], _ = strconv.ParseFloat(record[4], 64)
		ltv[2], _ = strconv.ParseFloat(record[5], 64)
		ltv[3], _ = strconv.ParseFloat(record[6], 64)
		ltv[4], _ = strconv.ParseFloat(record[7], 64)
		ltv[5], _ = strconv.ParseFloat(record[8], 64)
		ltv[6], _ = strconv.ParseFloat(record[9], 64)

		out = append(out, LTV{
			UserId:     record[0],
			CampaignID: record[1],
			Country:    record[2],
			LTV:        ltv,
		})
	}
	return out, nil
}

func toCampaignRevenueList(ltvs []LTV) service.RevenueList {
	revenueList := make(service.RevenueList)

	for _, ltv := range ltvs {
		ltv.shiftLTV()
		if revenue, ok := revenueList[ltv.CampaignID]; !ok {
			revenueList[ltv.CampaignID] = ltv.LTV
		} else {
			for i := 0; i < 7; i++ {
				revenue[i] += ltv.LTV[i]
			}
			revenueList[ltv.CampaignID] = revenue
		}
	}
	return revenueList
}

func toCountryRevenueList(ltvs []LTV) service.RevenueList {
	revenueList := make(service.RevenueList)

	for _, ltv := range ltvs {
		ltv.shiftLTV()
		if revenue, ok := revenueList[ltv.Country]; !ok {
			revenueList[ltv.Country] = ltv.LTV
		} else {
			for i := 0; i < 7; i++ {
				revenue[i] += ltv.LTV[i]
			}
			revenueList[ltv.Country] = revenue
		}
	}
	return revenueList
}

// Так как ltv1, ltv2 и так далее непосредственно в таблице относятся к пользователям, и в некоторых случаях ltv4-7 отсутствуют,
// можно сделать вывод, что пользователь пришёл через несколько дней с начала момента рассматриваемого периода в 7 дней.
// Если же рассматривать контекст прибыли компании или страны, то прибыль она получает в тот день (например ltv4 рассматриваемого периода),
// когда пришёл пользователь и что-то купил(а для пользователя это будет запись о ltv1).
// Исходя из этих соображений я решил сдвинуть массив ltv вправо, чтобы было понятно, в какой день пользователь пришёл и принёс прибыль
//в контексе компании или страны.
func (u *LTV) shiftLTV() {
	var k int
	for i := 0; i < 7; i++ {
		if u.LTV[i] == 0 {
			k = i
			break
		}
	}
	if k == 0 {
		return
	}
	shift := 7 - k

	var out [7]float64
	for i := k - 1; i >= 0; i-- {
		out[i+shift] = u.LTV[i]
	}
	u.LTV = out
}
