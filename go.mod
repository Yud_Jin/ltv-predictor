module gitlab.com/Yud_Jin/ltv-predictor

go 1.19

require (
	github.com/jinzhu/configor v1.2.1
	github.com/sirupsen/logrus v1.9.0
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
